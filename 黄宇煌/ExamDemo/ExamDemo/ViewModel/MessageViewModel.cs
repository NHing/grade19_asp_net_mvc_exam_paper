﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamDemo.ViewModel
{
    public class MessageViewModel
    {
        public int Id { get; set; }

        public string Content { get; set; }
        public IEnumerable<CommentViewModel> CommentViewModels { get; set; }
    }
}