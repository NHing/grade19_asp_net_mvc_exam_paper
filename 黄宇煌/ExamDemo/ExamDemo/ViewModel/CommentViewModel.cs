﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamDemo.ViewModel
{
    public class CommentViewModel
    {
        public string FromUserName { get; set; }
        public string ToUserName { get; set; }

        public string Comment { get; set; }
    }
}