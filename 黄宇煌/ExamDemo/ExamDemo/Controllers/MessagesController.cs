﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExamDemo.Data;
using ExamDemo.Models;
using ExamDemo.ViewModel;
using Newtonsoft.Json;
using ExamDemo.IndexModel;

namespace ExamDemo.Controllers
{
    public class MessagesController : Controller
    {
        private ExamDemoDb db = new ExamDemoDb();

        // GET: Messages
        public ActionResult Index()
        {
            var users = db.Users.ToList();
            var cmgs = db.Comments.ToList();
            var msgs = db.Messages.ToList();

            var msgViewModelList = new List<MessageViewModel>();

            foreach (var msg in msgs)
            {
                var cmgViewModelList = new List<CommentViewModel>();

                var tmpComments = cmgs.Where(x => x.MsgId == msg.Id).ToList();

                foreach (var cmg in tmpComments)
                {
                    var fromUser = users.Where(x => x.Id == cmg.FromUserId).FirstOrDefault();
                    var m = cmgs.Where(x => x.MsgId == cmg.MsgId).FirstOrDefault();
                    var toUser = users.Where(x => x.Id == m.FromUserId).FirstOrDefault();

                    cmgViewModelList.Add(new CommentViewModel
                    {
                        FromUserName = fromUser.UserName,
                        ToUserName = toUser.UserName,
                        Comment = cmg.Comment
                    });
                }


                msgViewModelList.Add(new MessageViewModel
                {
                    Id = msg.Id,
                    Content = msg.Content,
                    CommentViewModels = cmgViewModelList
                });
            }

            var vm = new SayViewModel
            {
                UserName = "只想简单的路过",
                MessageViewModels = msgViewModelList.OrderByDescending(x => x.Id)
            };
            return View(vm);

        }


        // GET: Messages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messages messages = db.Messages.Find(id);
            if (messages == null)
            {
                return HttpNotFound();
            }
            return View(messages);
        }

        // GET: Messages/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Messages/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性。有关
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FormUserId,Content,CreateAt,UpdateAt,Remarks")] Messages messages)
        {
            if (ModelState.IsValid)
            {
                db.Messages.Add(messages);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(messages);
        }

        // GET: Messages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messages messages = db.Messages.Find(id);
            if (messages == null)
            {
                return HttpNotFound();
            }
            return View(messages);
        }

        // POST: Messages/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性。有关
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FormUserId,Content,CreateAt,UpdateAt,Remarks")] Messages messages)
        {
            if (ModelState.IsValid)
            {
                db.Entry(messages).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(messages);
        }

        // GET: Messages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messages messages = db.Messages.Find(id);
            if (messages == null)
            {
                return HttpNotFound();
            }
            return View(messages);
        }

        // POST: Messages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Messages messages = db.Messages.Find(id);
            db.Messages.Remove(messages);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public string logout()
        {
            dynamic result;

            result = new
            {
                code = 200,
                msg = "注销成功"
            };
            return JsonConvert.SerializeObject(result);
        }

        [HttpPost]
        public string Say(SayModel sayModel)
        {
            dynamic result;

            db.Messages.Add(new Messages
            {
                FormUserId = 1,
                Content = sayModel.sayMsg
            });
            db.SaveChanges();

            result = new
            {
                code = 200,
                data = new
                {
                    id = 1,
                    content = sayModel.sayMsg
                }
            };


            return JsonConvert.SerializeObject(result);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
