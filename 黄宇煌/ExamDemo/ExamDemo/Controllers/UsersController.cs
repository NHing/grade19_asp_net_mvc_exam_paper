﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExamDemo.Data;
using ExamDemo.Models;
using ExamDemo.ParamModel;
using Newtonsoft.Json;

namespace ExamDemo.Controllers
{
    public class UsersController : Controller
    {
        private ExamDemoDb db = new ExamDemoDb();

        // GET: Users
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性。有关
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserName,PassWord,CreateAt,UpdateAt,Remarks")] Users users)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(users);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(users);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // POST: Users/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性。有关
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserName,PassWord,CreateAt,UpdateAt,Remarks")] Users users)
        {
            if (ModelState.IsValid)
            {
                db.Entry(users).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(users);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Users users = db.Users.Find(id);
            db.Users.Remove(users);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public string LoginDone(LoginModel loginModel)
        {
            dynamic result;

            var username = loginModel.UserName.Trim();
            var password = loginModel.PassWord.Trim();

            if (username.Length > 0 && password.Length > 0)
            {
                var searchUsername = db.Users.Where(x => x.UserName == username && x.PassWord == password).FirstOrDefault();
                //var searchPassword = db.Users.Where(x => x.PassWord == password);
                if (searchUsername != null)
                {
                    
                    result = new
                    {
                        code = 200,
                        msg = "登陆成功。。"
                    };
                }
                else
                {
                    result = new
                    {
                        code = 1000,
                        msg = "用户名或密码错误"
                    };
                }
            }
            else
            {
                result = new
                {
                    code = 1000,
                    msg = "请确认账号密码不为空"
                };
            }

            return JsonConvert.SerializeObject(result);
        }
        [HttpPost]
        public string RegisterDone(RegisterModel registerModel)
        {
            dynamic result;

            var username = registerModel.UserName.Trim();
            var password = registerModel.PassWord.Trim();
            var confirmpassword = registerModel.ConfirmPassWord.Trim();

            if (username.Length > 0 && password.Length > 0 && password == confirmpassword)
            {
                var users = db.Users.Where(x => x.UserName == username).FirstOrDefault();

                if (users == null)
                {
                    db.Users.Add(new Users
                    {
                        UserName = username,
                        PassWord = password
                    });

                    db.SaveChanges();
                    result = new
                    {
                        code = 200,
                        msg = "注册成功"
                    };
                }
                else
                {
                    result = new
                    {
                        code = 1000,
                        msg = "用户已经注册"
                    };
                }

            }
            else
            {
                result = new
                {
                    code = 1000,
                    msg = "用户名或密码不能为空，且两次密码需一致。。。"
                };
            }
            return JsonConvert.SerializeObject(result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
