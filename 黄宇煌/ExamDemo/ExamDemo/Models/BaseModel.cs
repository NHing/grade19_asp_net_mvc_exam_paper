﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamDemo.Models
{
    public class BaseModel
    {
        public BaseModel()
        {
            CreateAt = DateTime.Now;
            UpdateAt = DateTime.Now;
        }
        public int Id { get; set; }

        public DateTime CreateAt { get; set; }
        public DateTime UpdateAt { get; set; }
        public string Remarks { get; set; }
    }
}