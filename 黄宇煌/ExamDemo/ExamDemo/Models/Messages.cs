﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamDemo.Models
{
    public class Messages:BaseModel
    {
        public int FormUserId { get; set; }

        public string Content { get; set; }
    }
}