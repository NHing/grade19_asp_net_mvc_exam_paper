﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ExamDemo.Data;
using ExamDemo.Models;

namespace ExamDemo.Data
{
    public class DataInitlizer:DropCreateDatabaseIfModelChanges<ExamDemoDb>
    {
        protected override void Seed(ExamDemoDb db)
        {
            db.Users.AddRange(new List<Users>
            {
                new Users
                {
                    UserName="小明",
                    PassWord = "123"
                },
                new Users
                {
                    UserName="小红",
                    PassWord = "12321323"
                },
                new Users
                {
                    UserName="小栈",
                    PassWord = "wdsad"
                },
                new Users
                {
                    UserName="德瓦",
                    PassWord = "1usss23"
                },
                new Users
                {
                    UserName="刘宇",
                    PassWord = "1111123"
                },
                new Users
                {
                    UserName="吴柏",
                    PassWord = "32323123"
                },
                new Users
                {
                    UserName="若梦",
                    PassWord = "ssss123"
                }
            });

            db.Messages.AddRange(new List<Messages>
            {
                new Messages
                {
                    FormUserId=1,
                    Content="大哈啊"
                },
                new Messages
                {
                    FormUserId=3,
                    Content="多数的你好啊"
                },
                new Messages
                {
                    FormUserId=2,
                    Content="圣诞袜你哈啊"
                },
                new Messages
                {
                    FormUserId=5,
                    Content="等我你哈啊"
                },
                new Messages
                {
                    FormUserId=4,
                    Content="大碗大碗你哈啊"
                },
                new Messages
                {
                    FormUserId=4,
                    Content="低洼低洼"
                },
                new Messages
                {
                    FormUserId=6,
                    Content="时代的啊"
                },
            });

            db.Comments.AddRange(new List<Comments>
            {
                new Comments
                {
                    FromUserId=2,
                    MsgId=1,
                    Comment="是的啊"
                },
                new Comments
                {
                    FromUserId=1,
                    MsgId=2,
                    Comment="不要"
                },
                new Comments
                {
                    FromUserId=3,
                    MsgId=2,
                    Comment="达瓦"
                },
                new Comments
                {
                    FromUserId=2,
                    MsgId=4,
                    Comment="我带你们"
                },
                new Comments
                {
                    FromUserId=4,
                    MsgId=5,
                    Comment="可以"
                },
                new Comments
                {
                    FromUserId=7,
                    MsgId=7,
                    Comment="不是"
                },
                new Comments
                {
                    FromUserId=6,
                    MsgId=6,
                    Comment="低洼倒萨"
                }
            });
            db.SaveChanges();

            base.Seed(db);
        }
    };
}